
let arrX = ['a','b','c'];
let arrY = [1 , 2 , 3]; 
let arrC = ['js', 'css', 'jq'];


let arrZ = arrX.concat(arrY) ;
document.write ('Перше завдання : ');
document.write (`Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.`);
document.write (`<br>`);
for (let i = 0; i < arrZ.length ; i++){
    document.write(arrZ[i]);
    if ( i < arrZ.length - 1) {
        document.write(`,`);  
    }
}
document.write (`<br>`);
document.write ('Перше завдання : ');
document.write (`Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом. <br> Інший метод`);
document.write (`<br>`);
document.write(arrZ.toString());
document.write (`<br>`);


document.write (`<br>`);
document.write (`Друге завдання :`);
document.write (`Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.`);
document.write (`<br>`);
arrX.push(1 , 2 , 3 );
document.write(arrX.toString());
document.write (`<br>`);
arrX = ['a','b','c'];

document.write (`<br>`);
document.write (`Трете завдання :`);
document.write (`Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].`);
document.write (`<br>`);
document.write(arrY.reverse());
document.write (`<br>`);
arrY = [1 , 2 , 3]; 

document.write (`<br>`);
document.write (`Чертверте завдання :`);
document.write (`Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.`);
document.write (`<br>`);
arrY.push(4 , 5 , 6 );
document.write(arrY.toString());
document.write (`<br>`);
arrY = [1 , 2 , 3]; 

document.write (`<br>`);
document.write (`П'яте завдання :`);
document.write (`Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.`);
document.write (`<br>`);
arrY.unshift(4 , 5 , 6 );
document.write(arrY.toString());
document.write (`<br>`);
arrY = [1 , 2 , 3];

document.write (`<br>`);
document.write (`Шосте завдання :`);
document.write (`Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.`);
document.write (`<br>`);
document.write(arrC.shift());
document.write (`<br>`);

arrX = [1 , 2 , 3, 4, 5 ]; 
document.write (`<br>`);
document.write (`Сьоме завдання :`);
document.write (`Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].`);
document.write (`<br>`);
document.write(arrX.slice(0,3).toString());
document.write (`<br>`);

arrX = [1 , 2 , 3, 4, 5 ]; 
document.write (`<br>`);
document.write (`Восьме завдання :`);
document.write (`Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].`);
document.write (`<br>`);
arrX.splice(1,2);
document.write(arrX.toString());
document.write (`<br>`);

arrX = [1 , 2 , 3, 4, 5 ]; 
document.write (`<br>`);
document.write (`Девьяте завдання :`);
document.write (`Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].`);
document.write (`<br>`);
arrX.splice(2,0,10 );
document.write(arrX.toString());
document.write (`<br>`);

document.write (`<br>`);
document.write (`Десяте завдання :`);
document.write (`Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.`);
document.write (`<br>`);
arrX = [3, 4, 1, 2, 7];
arrX.sort();
document.write(arrX.toString());
document.write (`<br>`);

document.write (`<br>`);
document.write (`Одинадцяте завдання :`);
document.write (`Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, світ!'.`);
document.write (`<br>`);
arrX = ['Привіт, ' , 'світ' , '!']
for (let i = 0; i < arrX.length ; i++){
    document.write(arrX[i]);
}
document.write (`<br>`);

document.write (`<br>`);
document.write (`Дванадцяте завдання :`);
document.write (`Дан масив ['Привіт, ', 'світ', '!']. Необхідно записати в нульовий елемент цього масиву слово 'Поки, ' (тобто замість слова 'Привіт, ' буде 'Поки, ').`);
document.write (`<br>`);
arrX = ['Привіт, ' , 'світ' , '!']
arrX.splice(0,1,'Поки');
document.write(arrX.toString());
document.write (`<br>`);


document.write (`<br>`);
document.write (`Тринадцяте завдання :`);
document.write (`Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.`);
document.write (`<br>`);
let x = 1; 
let y = 2;
let z = 3;
let c = 4;
let v = 5;
let arrFirstWay =[x,y,z,c,v];
document.write (`Перший, через прямый запис елементів в масив :`);
document.write(arrFirstWay.toString());
document.write (`<br>`);
document.write (`Другий, через масив :`);
let arrNextWay =new Array(); 
for (let i = 1; i < 6 ; i++){
    arrNextWay[i] = i ;
}
document.write(arrNextWay.toString());
document.write (`<br>`);

document.write (`<br>`);
document.write (`Чотирнадцяте завдання :`);
document.write (`Дан багатовимірний масив arr.Виведіть за його допомогою слово 'блакитний' 'blue' .`);
document.write (`<br>`);
let arr = {
    'ru':['блакитний', 'червоний', 'зелений'],
    'en':['blue', 'red', 'green'],
};
document.write(`${arr.ru[0].toString()} , ${arr.en[0].toString()}`);
document.write (`<br>`);

document.write (`<br>`);
document.write (`П'ятнадцяте завдання :`);
document.write (`Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'.`);
document.write (`<br>`);
arr = ['a', 'b', 'c', 'd'];
document.write(`${arr[0].toString()}+${arr[1].toString()}, ${arr[2].toString()}+${arr[3].toString()}`);
document.write (`<br>`);

document.write (`<br>`);
document.write (`Шістнадцяте завдання :`);
document.write (`Запитайте у користувача кількість елементів масиву. Виходячи з даних, які ввів користувач створіть масив на ту кількість елементів, яку передав користувач. у кожному індексі масиву зберігайте чило який показуватиме номер елемента масиву.`);
document.write (`<br>`);
let arrFromUser = [];
let numberFromUser ;
numberFromUser = prompt('Ведіть число,яке потім створить масив з такою кількістю елементів', numberFromUser );
for (let i = 0; i < numberFromUser ; i++){
    arrFromUser[i] = i+1 ;
}
document.write(arrFromUser.toString());
document.write (`<br>`);



document.write (`<br>`);
document.write (`Сімнадцяте завдання :`);
document.write (`Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом.`);
document.write (`<br>`);
for (let i = 0; i < arrFromUser.length  ; i++){
    if(i%2 == 0){
        document.write(`<p> ${arrFromUser[i].toString()} </p>`);
    }
    if(i % 2 == 1){
        document.write(`<span style="background-color:rgba(223, 26, 26, 0.644);"> ${arrFromUser[i].toString()} </span>`);
    }
}
document.write (`<br>`); 


document.write (`<br>`);
document.write (`Вісімнадцяте завдання :`);
document.write (`Напишіть код, який перетворює та об'єднує всі елементи масиву в одне рядкове значення. Елементи масиву будуть розділені комою.`);
document.write (`<br>`);
let vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
let str1;
str1 =`${vegetables[0]}, ${vegetables[1]}, ${vegetables[2]}, ${vegetables[3]}`;
document.write(str1);
document.write (`<br>`); 